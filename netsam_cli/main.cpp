#include <chrono>
#include <iostream>

#include "netsam/connection.hpp"
#include "netsam/server.hpp"

#pragma pack(1)
struct message_header {
    uint64_t body_length = 0;
};
#pragma pack()

struct simple_message {
    using header_type = message_header;
    using body_type = std::string;

    header_type header;
    body_type body;

    simple_message()
    {
    }

    simple_message(const std::string& message)
        : body { message }
    {
        header.body_length = body.length();
    }

    void print()
    {
        std::cout << body << std::endl;
    }
};

void run_server()
{
    std::cout << "  run server mode" << std::endl;

    asio::io_context io_context;
    // auto work_guard = asio::make_work_guard(io_context);

    netsam::server<simple_message> server(io_context, 1234);
    server.set_on_accept([](auto conn) {
        conn->set_message_handler([conn](auto msg) {
            msg->print();
            conn->send(msg);
        });
    });

    using namespace std::chrono_literals;
    io_context.run_for(30s);

    std::cout << "  exit server mode" << std::endl;
}

void run_client()
{
    std::cout << "  run client mode" << std::endl;

    asio::io_context io_context;

    auto connection = std::make_shared<netsam::connection<simple_message>>(io_context, "127.0.0.1", 1234);
    connection->set_message_handler([](auto msg) {
        msg->print();
    });

    connection->start_listening();

    connection->send(std::make_shared<simple_message>("hallo"));
    connection->send(std::make_shared<simple_message>("hallo again"));

    io_context.run();

    std::cout << "  exit client mode" << std::endl;
}

int main(int argc, char* argv[])
{
    std::cout << "hello netsam" << std::endl;

    std::vector<std::string> args(argv, argv + argc);

    bool run_as_server = std::any_of(args.cbegin(), args.cend(),
        [](const auto& x) -> bool { return x == "-s"; });

    if (run_as_server) {
        run_server();
    } else {
        run_client();
    }
}