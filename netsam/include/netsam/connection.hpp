#pragma once

#include <cstdint>
#include <memory>
#include <string>

#include "asio.hpp"

#include "netsam/logging.hpp"

namespace netsam {

template <typename TMessage>
class connection : public std::enable_shared_from_this<connection<TMessage>> {

    static_assert(std::is_trivially_copyable<TMessage::header_type>(),
        "header type needs a fixed size, so has to be copyable by memcopy");

    asio::ip::tcp::socket socket_;
    logging::logger log_;
    std::function<void(std::shared_ptr<TMessage>)> message_handler_;

    /**
     * register send handler
     * 
     * @param message that should be sent
     */
    void do_send_message(std::shared_ptr<TMessage> message)
    {
        log_.debug("do_send_message");

        std::vector<asio::const_buffer> buffers;
        buffers.push_back(asio::buffer(&(message->header), sizeof(TMessage::header)));
        buffers.push_back(asio::buffer(message->body));
        auto self(shared_from_this());

        asio::async_write(
            socket_,
            buffers,
            [this, self, message](std::error_code ec, size_t bytes) {
                if (ec) {
                    log_.error("do_send_message got:", ec);
                } else {
                    log_.debug("  message sent");
                }
            });
    }

    /**
     * register handler for an incoming message header
     */
    void do_read_header()
    {
        log_.debug("do_read_header");

        auto message = std::make_shared<TMessage>();
        auto buffer = asio::buffer(&(message->header), sizeof(TMessage::header));
        auto self(shared_from_this());

        asio::async_read(
            socket_,
            buffer,
            [this, self, message](std::error_code ec, size_t bytes) {
                if (!ec) {
                    log_.debug("received message header");
                    do_read_body(message);
                } else if (ec == asio::stream_errc::eof) {
                    log_.debug(" -> connection closed");
                } else {
                    log_.error("do_read_header got: ", ec);
                }
            });
    }

    /**
     * register handler for an incoming message body
     * 
     * @param message instance that contains the header an will be filled
     */
    void do_read_body(std::shared_ptr<TMessage> message)
    {
        log_.debug("do_read_body");
        log_.debug("  read " + std::to_string(message->header.body_length) + "bytes");

        message->body.resize(message->header.body_length);
        auto self(shared_from_this());

        asio::async_read(
            socket_,
            asio::buffer(message->body),
            asio::transfer_exactly(message->header.body_length),
            [this, self, message](std::error_code ec, size_t bytes) {
                if (!ec) {
                    log_.debug("received message body");
                    do_read_header();
                    message_handler_(message);
                } else if (ec == asio::stream_errc::eof) {
                    log_.debug(" -> connection closed");
                } else {
                    log_.error("do_read_body got: ", ec);
                }
            });
    }

public:
    connection(asio::ip::tcp::socket&& socket, logging::logger logger)
        : socket_ { std::move(socket) }
        , log_(logger)
    {
        log_.debug("created connection for: "
            + socket_.remote_endpoint().address().to_string() + ":"
            + std::to_string(socket_.remote_endpoint().port()));
    }

    connection(asio::io_context& io_context, std::string hostname, uint16_t port, logging::log_callback log_callback = logging::console_log)
        : socket_(io_context)
        , log_(log_callback)
    {
        log_.debug("creating connection for: "
            + hostname + ":"
            + std::to_string(port));

        auto address = asio::ip::address::from_string(hostname);
        asio::ip::tcp::endpoint endpoint(address, port);

        std::error_code ec;
        socket_.connect(endpoint, ec);

        if (ec) {
            log_.error("  connect returned: ", ec);
        } else {
            log_.debug("  connected");
        }
    };

    virtual ~connection()
    {
        log_.debug("destroy connection");
    }

    void start_listening()
    {
        do_read_header();
    }

    void set_message_handler(std::function<void(std::shared_ptr<TMessage>)> message_handler)
    {
        message_handler_ = message_handler;
    }

    void send(std::shared_ptr<TMessage> message)
    {
        //log_.debug("send: " + message);
        do_send_message(message);
    }
};

} // namespace netsam