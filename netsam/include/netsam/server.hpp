#pragma once

#include <cstdint>
#include <memory>

#include "asio.hpp"

#include "netsam/connection.hpp"
#include "netsam/logging.hpp"

namespace netsam {

template <typename TMessage>
class server {
    asio::ip::tcp::acceptor acceptor_;
    logging::logger log_;
    std::function<void(std::shared_ptr<connection<TMessage>>)> on_accept_;

    void do_accept()
    {
        acceptor_.async_accept(
            [this](std::error_code ec, asio::ip::tcp::socket socket) {
                if (!ec) {
                    log_.debug("incomming connection from: "
                        + socket.remote_endpoint().address().to_string() + ":"
                        + std::to_string(socket.remote_endpoint().port()));

                    auto conn = std::make_shared<connection<TMessage>>(std::move(socket), log_);
                    on_accept_(conn);
                    conn->start_listening();
                }

                do_accept();
            });
    }

public:
    server(asio::io_context& io_context, uint16_t port, logging::log_callback log_callback = logging::console_log)
        : acceptor_(io_context,
            asio::ip::tcp::endpoint(asio::ip::tcp::v4(), port))
        , log_(log_callback)
    {
        do_accept();
    }

    void set_on_accept(std::function<void(std::shared_ptr<connection<TMessage>>)> on_accept)
    {
        on_accept_ = on_accept;
    }
};

} // namespace netsam