#pragma once

#include <functional>
#include <iomanip>
#include <iostream>
#include <string>
#include <system_error>

namespace netsam::logging {

/** 
 * Log severity similar to syslog.
 * 
 * https://tools.ietf.org/html/rfc5424#section-6.2.1 * 
 */
enum class severity {
    emerg = 0,
    alert = 1,
    critical = 2,
    error = 3,
    warning = 4,
    notice = 5,
    informational = 6,
    debug = 7
};

std::ostream& operator<<(std::ostream& os, severity sev)
{
    switch (sev) {
    case severity::emerg:
        os << "emergency";
        break;
    case severity::alert:
        os << "alert";
        break;
    case severity::critical:
        os << "critical";
        break;
    case severity::error:
        os << "error";
        break;
    case severity::warning:
        os << "warning";
        break;
    case severity::notice:
        os << "notice";
        break;
    case severity::informational:
        os << "info";
        break;
    case severity::debug:
        os << "debug";
        break;
    default:
        break;
    }
    return os;
}

using log_callback = std::function<void(severity, const std::string&)>;

void console_log(severity severity, const std::string& message)
{
    std::cout << std::setw(10) << severity << " - " << message << std::endl;
}

class logger {
    log_callback log_callback_;

public:
    logger(log_callback log_method)
        : log_callback_(log_method)
    {
    }

    void debug(const std::string& message)
    {
        log_callback_(severity::debug, message);
    }

    void error(const std::string& message)
    {
        log_callback_(severity::error, message);
    }

    void error(const std::string& message, const std::error_code& ec)
    {
        error(message + ec.message());
    }
};

}
